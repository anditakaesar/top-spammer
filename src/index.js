import express from 'express'

const app = express()

app.use('/', (req, res) => {
    res.status(200).json({
        message: 'hello world'
    })
})

app.listen(3004, () => {
    console.log('app listen at port', 3004)
})