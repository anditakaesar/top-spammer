const gulp = require("gulp")
const babel = require('gulp-babel')

gulp.task('default', (cb) => {
    console.log('TODO: default gulp task')
    cb()
})

function build(cb) {
    gulp.src(['src/**/*.js'])
        .pipe(babel({
            presets: ['@babel/preset-env']
        }))
        .pipe(gulp.dest('dist'))
    cb()
}

gulp.task('watch', (cb) => {
    gulp.watch(['src/**/*.js'], build)
    cb()
})

gulp.task('build', build)